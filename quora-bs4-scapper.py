import feedparser, hashlib, json, urllib2
from pprint import pprint
from bs4 import BeautifulSoup 
from HTMLParser import HTMLParser
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import sys
#data dictionaries storing data
#dictionary containing questions asked by the user along with topics of question and number of views and followers
quesAsked = {'title':[],'ques_link':[],'topics':[],'ques_views':[],'ques_follow':[]}
#dictionary of questions answered by the user with their link, views and followers
quesAnswered = {'title':[],'ques_link':[],'ans_link':[],'answers':[],'topics':[],'ques_views':[],'ques_follow':[],'ans_views':[],'ans_vote':[]}
#dictionary of questions followed by the user
quesFollowed = {'title':[],'ques_link':[],'topics':[],'ques_views':[],'ques_follow':[]}
#dictionary of answers voted up by the user
ansUpvote = {'title':[],'ques_link':[],'ans_link':[],'answers':[],'topics':[],'ques_views':[],'ques_follow':[],'ans_views':[],'ans_vote':[]}

info = {'Questions':0,'Answers':0,'Posts':0,'Name':'','discription':'','social_handle':[]}

ques_links = []
ques_links_id = []
data = []


class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ';'.join(self.fed)

def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data() 

def processData(url):
    '''returns list of complete paths for Downloading'''
    content = urllib2.urlopen(url).read()
    soup = BeautifulSoup(content)
    return soup

class profile(object):
    """Name, Photo Download, No. of Questions, Answers and Posts,social links,brief description"""
    def __init__(self, url,driver):
        super(profile, self).__init__()
        self.url = url
        self.driver = driver 
        self.soup = self.processData(self.url)
        self.info = self.basicProfile(self.soup) #data is a json string
        info = self.info
        self.writeToJSON(self.info, str(self.info['Name'])+'.json')

    def processData(self, url):
        '''returns list of complete paths for Downloading'''
        driver = self.driver
        driver.get(url)
        html_source = driver.page_source      
        soup = BeautifulSoup(html_source,'html.parser')
        # content = urllib2.urlopen(url).read()
        # soup = BeautifulSoup(content)
        return soup

    def basicProfile(self,soup):
        '''returns basic information about user as json'''
        name = soup.title.string.split('-')[0].strip() #UserName
        #name_s = name.split()
        #name = ' '.join(name_s[1:])
        # profilePicURL = [x['src'] for x in soup.findAll('img', {'class': 'profile_photo_img'})][0]
        for dd in soup.findAll('div',{'class':'ProfilePhoto'}):
            ii  = dd.findAll('img',{'class':'profile_photo_img'})
            for j in ii:
                    profilePicURL =  str(j.attrs['src'])
        # print profilePicURL
        f = urllib2.urlopen(profilePicURL)
        # Profile Picture Downloaded
        filename = name + ".jpeg"        
        with open(filename, "wb") as image:
            image.write(f.read())
        d = soup.findAll('div',{'class':'ProfileNameAndSig'})
        d2 = strip_tags(str(d)).split(';')
        disc = d2
        link = []
        for t  in soup.findAll('div',{'class':'action_item'}):
            a = t.findAll('a')
            for q in a:
                if q.text.strip() =='Twitter':
                    link.append(q.attrs['href'])
                if q.text.strip() =='Facebook':
                    link.append(q.attrs['href'])
        activityStats = soup.findAll('span', {'class': 'profile_count'})
        # activity = {}
        info['Questions']   =   int(activityStats[0].string)
        info['Answers']     =   int(activityStats[1].string)
        info['Posts']       =   int(activityStats[2].string)
        info['Name']        =   name
        info['discription'] =   disc
        info['social_handle'] = link
        return info

    def writeToJSON(self, info, filename):
        '''writes data to JSON File'''
        jFile = open(filename, 'w+')
        jFile.write(json.dumps(info))
        jFile.close()

def getansvnf(driver,dict_name):
    '''get the views and upvotes for an answer'''
    for url in dict_name["ans_link"]:
        cur_time = time.time()
        driver.get(url)
        html_source = driver.page_source
        soup = BeautifulSoup(html_source,'html.parser')
	print url
        print 'rendering '+ str(time.time() - cur_time)
	cur_time = time.time()
        v = soup.findAll('span',{'class':'stats_row'})
        views =  strip_tags(str(v)).split(';')
        f = soup.findAll('span',{'class':'vote_item_link_wrapper'})
        follow = strip_tags(str(v)).split(';')
        dict_name["ans_views"].append(str(views))
        dict_name["ans_vote"].append(str(follow))
	print 'scrapp '+str(time.time()-cur_time)

def getvnf(driver,dict_name):
    '''get the views and followers of questions and the tpics f the questions'''
    for url in dict_name["ques_link"]:
        cur_time = time.time()
        driver.get(url)
        html_source = driver.page_source
        soup = BeautifulSoup(html_source,'html.parser')
	print url
        print 'rendering '+ str(time.time()-cur_time)
	cur__time = time.time()
        try:
            div = soup.find('div',{'class':'grid_page_left_col'})
            v = div.findAll('span',{'class':'stats_row'})
            f = div.findAll('a',{'class':'stats_row'})
            views =  strip_tags(str(v)).split(';')[2]
            follow =  strip_tags(str(f)).split(';')[2]
            t = div.findAll('span', {'class': 'TopicName'})
            Topics = []
            for i in t:
                val = strip_tags(str(i))
                if val not in Topics:
                    Topics.append(val)
            dict_name["topics"].append(Topics)
            dict_name["ques_views"].append(str(views))
            dict_name["ques_follow"].append(str(follow))
	    print 'scrap '+str(time.time()-cur_time)
        except Exception,e:
            print 'failed for'+url 
def abc(driver):
    '''get the views and followers of questions and the tpics f the questions'''

    for url in ques_links:
        driver.get(url)
        html_source = driver.page_source
        soup = BeautifulSoup(html_source,'html.parser')
        v = soup.findAll('span',{'class':'stats_row'})
        f = soup.findAll('a',{'class':'stats_row'})
        views =  strip_tags(str(v)).split(';')[2]
        follow =  strip_tags(str(f)).split(';')[2]
        t = soup.findAll('span', {'class': 'TopicName'})
        Topics = []
        for i in t:
            val = strip_tags(str(i))
            if val not in Topics:
                Topics.append(val)
        x = {"url":'',"topics":[],"views":'',"follow":''}
        x["url"] = url
        x["topics"] = (Topics)
        x["views"] = (str(views))
        x["follow"]= (str(follow))
        data.append(x)


def askedQuestions(url):
    '''Questions asked'''
    d = feedparser.parse(url+"/questions/rss")
    for entry in d["entries"]:
        # print "check"
        title = entry["title"].encode("utf-8")
        h = hashlib.sha224(title).hexdigest()
        quesAsked["title"].append(title)
        link = entry["link"].encode("utf-8")
        h = hashlib.sha224(link).hexdigest()
        quesAsked["ques_link"].append(link)
        # print entry.keys()
        # print d.keys()
        # break
        # if int(enrty["id"]) not in ques_links_id:
        #     ques_links_id.append(int(enrty["id"]))
        #     ques_links.append(link)

def quesAnswer(url):
    '''answers written'''
    d = feedparser.parse(url+"/answers/rss")
    for e in d["entries"]:
        title = e["title"].encode("utf-8")
        h = hashlib.sha224(title).hexdigest()
        summary = e["summary"].encode("utf-8")
        link = e["link"].encode("utf-8")
        h = hashlib.sha224(link).hexdigest()
        quesAnswered["title"].append(title)
        quesAnswered["answers"].append(summary)
        quesAnswered["ans_link"].append(link)
        lst = link.split('/')
        del lst[len(lst)-1]
        del lst[len(lst)-1]
        link = '/'.join(lst)
        quesAnswered["ques_link"].append(link)
        # if link not in ques_links:
        #     ques_links.append(link)

def Timeline(url):
    '''followed questions and voted up answers'''
    d = feedparser.parse(url+"/rss")
    for e in d["entries"]:
        title = e["title"].encode("utf-8")
        h = hashlib.sha224(title).hexdigest()
        link = e["link"].encode("utf-8")
        h = hashlib.sha224(link).hexdigest()
        summary = e["summary"].encode("utf-8")
        val = strip_tags(summary)
        lst = val.split(';')
        if lst:
            if  lst[0].find("voted up this answer") > 0:
                ansUpvote["ans_link"].append(link)
                lst1 = link.split('/')
                del lst1[len(lst1)-1]
                del lst1[len(lst1)-1]
                link = '/'.join(lst1)
                ansUpvote["title"].append(title)
                ansUpvote["answers"].append(summary)
                ansUpvote["ques_link"].append(link)
                # if link not in ques_links:
                #     ques_links.append(link)
            if  lst[0].find("followed a question") > 0:
                quesFollowed["title"].append(title)
                quesFollowed["ques_link"].append(link)
                # if link not in ques_links:
                    # ques_links.append(link)

if __name__ == '__main__':
    #lst = sys.argv
    #length = len(lst)
    #if length !=4:
     #   print "please enter the arguments in proper format as 'python filename.py email password profile_url_to_scrap' "
     #   sys.exit(2)
    #email = str(raw_input('enter the email-address: '))
   # password  = str(raw_input('enter the password: '))
    start_time = time.time()
    print 'Wait.....'
    url = "http://www.quora.com/"
    try:
        driver = webdriver.PhantomJS('phantomjs')
        driver.get(url)
        form = driver.find_element_by_class_name('inline_login_form')
        username = form.find_element_by_name('email')
        username.send_keys(str(raw_input('Quora Email id: ')))
        password = form.find_element_by_name('password')
        password.send_keys(str(raw_input('Password:')))
        password.send_keys(Keys.RETURN)
        print "sucess"
    except:
        print 'Login Failed!!!!!..... Try again.....'
        sys.exit(2)
    url_name = str(raw_input('enter the url id of the user whose data to be scrapped :'))
    time.sleep(10)
    url = url + url_name
    nk = profile(url,driver)
    print time.time() - start_time
    #askedQuestions(url)
    #print time.time() - start_time
    #quesAnswer(url)
   # print time.time() - start_time
    #Timeline(url)
    #print time.time() - start_time
    #getvnf(driver,quesAsked)
    #time.sleep(5)
    #print 'check1'
    #print time.time() - start_time
    #getvnf(driver,quesAnswered)
    #time.sleep(5)
    #print 'check2'
    #print time.time() - start_time
    #getvnf(driver,quesFollowed)
    #time.sleep(5)
    #print 'check3'
    #print time.time() - start_time
    # getvnf(driver,ansUpvote)
    # time.sleep(5)
    # print 'check4'
    # print time.time() - start_time
    # abc(driver)
    #print "questions done"
    # print time.time() - start_time
    #getansvnf(driver,ansUpvote)
    #time.sleep(5)
    #print 'check5'
    #print time.time() - start_time
    #getansvnf(driver,quesAnswered)
    #print 'check6'
    #print time.time() - start_time
    ## # print quesAsked
    # print quesAnswered
    jFile = open(info['Name']+'data.json', 'w+')
    jFile.write(json.dumps(quesAsked))
    jFile.write(json.dumps(quesAnswered))
    jFile.write(json.dumps(quesFollowed))
    jFile.write(json.dumps(ansUpvote))
    jFile.close()
    driver.close()
    #print time.time() - start_time
